package com.sda.Empik_Maven.utils;

import java.util.LinkedList;
import java.util.Map;

import com.sda.Empik_Maven.products.AvailableProductsCategory;
import com.sda.Empik_Maven.products.Product;
import com.sda.Empik_Maven.shop.Shop;



public class ShopBrowser {
	
	public static void browseCategories(Shop givenShop){
		Map<AvailableProductsCategory, LinkedList<Product>> map = givenShop.shopProductRange;
		System.out.println("Available categories: ");
		for(Map.Entry<AvailableProductsCategory, LinkedList<Product>> entry : map.entrySet()){
			System.out.println(entry.getKey().getID() + ") " + entry.getKey());
		}
	}
	
	public static void browseProductsInGivenCategory(AvailableProductsCategory givenCategory, Shop givenShop){
		System.out.println("Available products in category " + givenCategory + " :");
		for(Map.Entry<AvailableProductsCategory, LinkedList<Product>> entry : givenShop.shopProductRange.entrySet()){
			if(entry.getKey() == givenCategory){
				for(Product product : entry.getValue()){
					System.out.println(product);
				}
			}
		}		
	}
	
	

}
