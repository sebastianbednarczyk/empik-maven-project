package com.sda.Empik_Maven.runner;

import com.sda.Empik_Maven.products.AvailableProductsCategory;
import com.sda.Empik_Maven.products.Book;
import com.sda.Empik_Maven.products.CD;
import com.sda.Empik_Maven.products.Mug;
import com.sda.Empik_Maven.products.NewsPaper;
import com.sda.Empik_Maven.products.Poster;
import com.sda.Empik_Maven.products.ProductFactory;
import com.sda.Empik_Maven.shop.Shop;
import com.sda.Empik_Maven.users.Admin;
import com.sda.Empik_Maven.users.Customer;
import com.sda.Empik_Maven.utils.ShopBrowser;

public class Runner {

	public static void main(String[] args) {
		
		Shop myShop = Shop.getInstance();
		Admin myAdmin = Admin.returnAdminIfPasswordIsCorrect(666);
		Customer myCustomer = Customer.createNewCustomer("Sebastian", "Klient");
		
		Book book = (Book)ProductFactory.addNewBook("Ogniem i mieczem", 100, 500);
		CD cd = (CD) ProductFactory.addNewCD("Kanye West", 40, 2006);
		Mug mug = (Mug) ProductFactory.addNewMug("Kubek z kubusiem", 100);
		NewsPaper newspaper = (NewsPaper) ProductFactory.addNewNewsPaper("Gazeta", 20);
		Poster poster = (Poster) ProductFactory.addNewPoster("poster1", 40);
		Poster poster1 = (Poster) ProductFactory.addNewPoster("poster2", 30);
		Poster poster2 = (Poster) ProductFactory.addNewPoster("poster3", 20);
		myShop.addNewProductToShopProductRange(book, myAdmin);
		myShop.addNewProductToShopProductRange(cd, myAdmin);
		myShop.addNewProductToShopProductRange(mug, myAdmin);
		myShop.addNewProductToShopProductRange(newspaper, myAdmin);
		myShop.addNewProductToShopProductRange(poster, myAdmin);
		myShop.addNewProductToShopProductRange(poster1, myAdmin);
		myShop.addNewProductToShopProductRange(poster2, myAdmin);
		myCustomer.getCustomerCart().addToCart(book.getID());
		myCustomer.getCustomerCart().addToCart(cd.getID());
		myCustomer.getCustomerCart().addToCart(mug.getID());
		myCustomer.getCustomerCart().addToCart(poster1.getID());
		myCustomer.getCustomerCart().addToCart(poster2.getID());
		myCustomer.getCustomerCart().addToCart(poster.getID());
		
		ShopBrowser.browseCategories(myShop);
		System.out.println("?????????????????????");
		ShopBrowser.browseProductsInGivenCategory(AvailableProductsCategory.POSTER, myShop);
		System.out.println("?????????????????????");		
		myCustomer.getCustomerCart().displayCart();

	}

}
