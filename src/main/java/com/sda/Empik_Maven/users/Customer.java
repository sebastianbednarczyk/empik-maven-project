package com.sda.Empik_Maven.users;

import com.sda.Empik_Maven.cart.Cart;
import com.sda.Empik_Maven.shop.Shop;

public class Customer {
	
	private static int id = 100;
	private String name;
	private String lastName;
	
	private static Cart customerCart;
	
	private Customer(String name, String lastName) {
		id++;
		this.name = name;
		this.lastName = lastName;
	}
	
	public static Customer createNewCustomer(String name, String lastName){
		Customer newCustomer = new Customer(name, lastName);
		Shop.getInstance().Customers.put(newCustomer.id, newCustomer);
		customerCart = new Cart(newCustomer);
		return newCustomer;
	}
	
	public Cart getCustomerCart(){
		return customerCart;
	}

	public String getName() {		
		return name;
	}
	
	public String getLastName(){
		return lastName;
	}
	
	public int getID(){
		return id;
	}

}
