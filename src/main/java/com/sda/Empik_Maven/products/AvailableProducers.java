package com.sda.Empik_Maven.products;

public enum AvailableProducers {
	
	Ubisoft(1),
	EAGames(2),
	CDProjectRed(3);
	
	private int id;
	
	private AvailableProducers(int id) {
		this.id = id;
	}
	
	public int getID () {
		return id;
	}
	
	public static AvailableProducers choosingProducer(int id){
		AvailableProducers chosenProducer = null;
		for(AvailableProducers prod : AvailableProducers.values()){
			if(prod.getID() == id){
				chosenProducer = prod;
			}
		}
		return chosenProducer;
	}
	
	public static void showAvailableProducers(){
		for(AvailableProducers prod : AvailableProducers.values()){
			System.out.println(prod.getID() + ") " + prod);
		}
	}
}
