package com.sda.Empik_Maven.products;

public class Vinyl extends Product {
	
	int categoryID = 3;
	static int counter = 10;
	
	public Vinyl(AvailableProductsCategory productCategory, String givenName, int givenPrice) {
		super(productCategory);
		name = givenName;
		price = givenPrice;
		id = categoryID + "-" + counter;
		counter++;			
	}
	
	@Override
	public String toString() {
		return String.format( "%-5s | %-35s | $ %8d", id, name, price);
	}

	@Override
	public String getID() {
		return id;
	}
	
	@Override
	public AvailableProductsCategory getCategory() {
		return this.productCategory;
	}

	@Override
	public String getName() {		
		return this.name;
	}

	@Override
	public int getPrice() {
		return this.price;
	}

}
