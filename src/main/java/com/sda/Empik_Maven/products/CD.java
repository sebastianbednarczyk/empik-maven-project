package com.sda.Empik_Maven.products;

public class CD extends Product {

	int categoryID = 1;
	static int counter = 10;

	int productionYear;

	public void setProductionDate(int productionDate) {
		this.productionYear = productionDate;
	}
	
	public int getProductionDate(){
		return productionYear;
	}

	public CD(AvailableProductsCategory givenCategory, String givenName, int givenPrice, int givenProductionYear) {
		super(givenCategory);
		name = givenName;
		price = givenPrice;
		productionYear = givenProductionYear;
		id = categoryID + "-" + counter;
		counter++;		
	}

	public String getID() {
		return id;
	}

	@Override
	public String toString() {
		return String.format("%-5s | %-35s | $ %8d", id, String.format("%s, %d", name, productionYear), price);
	}

	@Override
	public AvailableProductsCategory getCategory() {
		return this.productCategory;
	}

	@Override
	public String getName() {		
		return this.name;
	}

	@Override
	public int getPrice() {
		return this.price;
	}

}
