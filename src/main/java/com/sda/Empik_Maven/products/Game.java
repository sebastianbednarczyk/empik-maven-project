package com.sda.Empik_Maven.products;

public class Game extends Product{
	
	int categoryID = 7;
	static int counter = 10;
	
	AvailableProducers producer;
	
	public void setProducer(AvailableProducers producer) {
		this.producer = producer;
	}
	
	public AvailableProducers getProducer(){
		return producer;
	}
	
	public Game(AvailableProductsCategory productCategory, String givenName, int givenPrice, AvailableProducers givenProducer) {
		super(productCategory);
		name = givenName;
		price = givenPrice;
		producer = givenProducer;
		id = categoryID + "-" + counter;
		counter++;	
	}
	
	@Override
	public String toString() {
		return String.format( "%-5s | %-35s | $ %8d", id, String.format("%s, %s", name, producer), price);
	}

	@Override
	public String getID() {
		return id;
	}
	@Override
	public AvailableProductsCategory getCategory() {
		return this.productCategory;
	}

	@Override
	public String getName() {		
		return this.name;
	}

	@Override
	public int getPrice() {
		return this.price;
	}
}
