package com.sda.Empik_Maven.products;

public class Book extends Product{
	
	int categoryID = 4;
	static int counter = 10;	
	int pages;
		
	public void setPages(int pages) {
		this.pages = pages;
	}
	
	public int getPages(){
		return pages;
	}

	public Book(AvailableProductsCategory productCategory, String givenName, int givenPrice, int givenPages) {
		super(productCategory);
		name = givenName;
		price = givenPrice;
		pages = givenPages;
		id = categoryID + "-" + counter;
		counter++;
	}
	
	@Override
	public String toString() {
		return String.format( "%-5s | %-35s | $ %8d", id, String.format("%s, ( %d pages)", name, pages), price);
	}

	@Override
	public String getID() {
		return id;
	}
	@Override
	public AvailableProductsCategory getCategory() {
		return this.productCategory;
	}

	@Override
	public String getName() {		
		return this.name;
	}

	@Override
	public int getPrice() {
		return this.price;
	}
}
