package com.sda.Empik_Maven.products;

public abstract class Product {
	
	AvailableProductsCategory productCategory;
	String id;
	String name;
	int price;
	
	public Product(AvailableProductsCategory productCategory) {
		this.productCategory = productCategory;
	}
	
	public abstract String getID();
	
	public abstract AvailableProductsCategory getCategory();
	
	public abstract String getName();
	
	public abstract int getPrice();
}
