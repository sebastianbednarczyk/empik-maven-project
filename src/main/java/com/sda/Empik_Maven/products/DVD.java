package com.sda.Empik_Maven.products;

public class DVD extends Product{
	
	int categoryID = 2;
	static int counter = 10;
	
	int productionYear;
	
	public void setProductionDate(int productionDate) {
		this.productionYear = productionDate;
	}
	
	public int getProductionDate(){
		return productionYear;
	}
	
	public DVD(AvailableProductsCategory productCategory, String givenName, int givenPrice, int givenProductionYear) {
		super(productCategory);
		name = givenName;
		price = givenPrice;
		productionYear = givenProductionYear;
		id = categoryID + "-" + counter;
		counter++;	
	}
	
	@Override
	public String toString() {
		return String.format( "%-5s | %-35s | $ %8d", id, String.format("%s, %d", name, productionYear), price);
	}

	@Override
	public String getID() {
		return id;
	}
	
	@Override
	public AvailableProductsCategory getCategory() {
		return this.productCategory;
	}

	@Override
	public String getName() {		
		return this.name;
	}

	@Override
	public int getPrice() {
		return this.price;
	}

}
