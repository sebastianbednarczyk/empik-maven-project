package com.sda.Empik_Maven.products;

public class ProductFactory {

	public static CD addNewCD(String givenName, int givenPrice, int givenProdcutionYear) {
		CD newCD = new CD(AvailableProductsCategory.CD, givenName, givenPrice, givenProdcutionYear);
		return newCD;
	}

	public static DVD addNewDVD(String givenName, int givenPrice, int givenProdcutionYear) {
		DVD newDVD = new DVD(AvailableProductsCategory.CD, givenName, givenPrice, givenProdcutionYear);

		return newDVD;
	}

	public static Vinyl addNewVinyl(String givenName, int givenPrice) {
		Vinyl NewVinyl = new Vinyl(AvailableProductsCategory.VINYL, givenName, givenPrice);
		return NewVinyl;
	}

	public static Book addNewBook(String givenName, int givenPrice, int givenPages) {
		Book NewBook = new Book(AvailableProductsCategory.BOOK, givenName, givenPrice, givenPages);
		return NewBook;
	}

	public static Poster addNewPoster(String givenName, int givenPrice) {
		Poster NewPoster = new Poster(AvailableProductsCategory.POSTER, givenName, givenPrice);
		return NewPoster;
	}

	public static Mug addNewMug(String givenName, int givenPrice) {
		Mug NewMug = new Mug(AvailableProductsCategory.MUG, givenName, givenPrice);
		return NewMug;
	}

	public static Game addNewGame(String givenName, int givenPrice, AvailableProducers producer) {
		Game NewGame = new Game(AvailableProductsCategory.GAME, givenName, givenPrice, producer);
		return NewGame;
	}

	public static NewsPaper addNewNewsPaper(String givenName, int givenPrice) {
		NewsPaper NewNewsPaper = new NewsPaper(AvailableProductsCategory.NEWSPAPER, givenName, givenPrice);
		return NewNewsPaper;
	}
}
