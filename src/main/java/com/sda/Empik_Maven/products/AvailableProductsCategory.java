package com.sda.Empik_Maven.products;

public enum AvailableProductsCategory {
	
	CD(1),
	DVD(2),
	VINYL(3),
	BOOK(4),
	POSTER(5),
	MUG(6),
	GAME(7),
	NEWSPAPER(8);
	
	int id;
	
	AvailableProductsCategory(int id){
		this.id = id;
	}
	
	public int getID(){
		return id;
	}
	
	public static void showAvailableProductCategory(){
		for(AvailableProductsCategory productCategory: AvailableProductsCategory.values()){
			System.out.println(productCategory.getID() + ") " + productCategory);
		}
	}
}
