package com.sda.Empik_Maven.cart;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sda.Empik_Maven.products.AvailableProductsCategory;
import com.sda.Empik_Maven.products.Book;
import com.sda.Empik_Maven.products.CD;
import com.sda.Empik_Maven.products.DVD;
import com.sda.Empik_Maven.products.Poster;
import com.sda.Empik_Maven.products.Product;
import com.sda.Empik_Maven.shop.Shop;



public class DiscountCalculator {

	Cart cart;

	public DiscountCalculator(Cart cart) {
		this.cart = cart;
	}
	/**
	 * This method use givenProduct id to find ProductInCart which was made using this same givenProduct.
	 * After that it sets ProductInCart.priceAfterDiscount to calculated discount price using CD/DVD age.
	 */
	public void discounterCDandDVD(Product givenProduct) {
		for (ProductInCart product : cart.bucketList) {
			if (product.orginalProduct.getID().equals(givenProduct.getID())) {
				AvailableProductsCategory productCategory = product.orginalProduct.getCategory();
				switch (productCategory) {
				case CD:
					CD parsedToCD = (CD) product.orginalProduct;
					int actualYear = Calendar.getInstance().get(Calendar.YEAR);
					int productProductionYear = parsedToCD.getProductionDate();
					int discountPercentSize = actualYear - productProductionYear;
					int orginalPrice = product.orginalProduct.getPrice();
					double priceAfterDiscount = orginalPrice - (orginalPrice * discountPercentSize / 100);
					product.priceAfterDiscount = priceAfterDiscount;
					break;
				case DVD:
					DVD parsedToDVD = (DVD) product.orginalProduct;
					actualYear = Calendar.getInstance().get(Calendar.YEAR);
					productProductionYear = parsedToDVD.getProductionDate();
					discountPercentSize = actualYear - productProductionYear;
					orginalPrice = product.orginalProduct.getPrice();
					priceAfterDiscount = orginalPrice - (orginalPrice * discountPercentSize / 100);
					product.priceAfterDiscount = priceAfterDiscount;
					break;
				}
			}
		}
	}

	public void discounterBook(Product givenProduct) {
		for (ProductInCart product : cart.bucketList) {
			if (product.orginalProduct.getID().equals(givenProduct.getID())) {
				Book parsedToBook = (Book) product.orginalProduct;
				int pages = parsedToBook.getPages();
				int parsedPages = pages / 100;
				if (parsedPages > 4) {
					parsedPages = 4;
				}
				int orginalPrice = product.orginalProduct.getPrice();
				double priceAfterDiscount = orginalPrice - (orginalPrice * parsedPages / 10);
				product.priceAfterDiscount = priceAfterDiscount;
			}
		}
	}

	public void discounterMug(Product givenProduct) {
		System.out.println("Special offert. When buying a Mug You get Newspaper for free!");
		List<Product> listOfAvailableNewspapers = Shop.getInstance().shopProductRange
				.get(AvailableProductsCategory.NEWSPAPER);
		int chosenRandomNewspaperIndex = (int) Math.floor(((Math.random()) * listOfAvailableNewspapers.size()));
		Product freeNewspaper = listOfAvailableNewspapers.get(chosenRandomNewspaperIndex);
		
		cart.addToCart(freeNewspaper.getID());
		for (ProductInCart lookingForNewspaper : cart.bucketList) {
			if (lookingForNewspaper.orginalProduct.getID().equals(freeNewspaper.getID())&&
					lookingForNewspaper.priceAfterDiscount != 0) {
				lookingForNewspaper.priceAfterDiscount = (double) 0;
				return;
			}
		}
	}

	public void discounterPoster(Product givenProduct) {
		
		List<ProductInCart> posterList = new ArrayList<>();
		for (ProductInCart product : cart.bucketList) {
			if (product.orginalProduct.getID().equals(givenProduct.getID())) {
				Poster parsedToPoster = (Poster) product.orginalProduct;
				product.priceAfterDiscount = parsedToPoster.getPrice();
				
			}
		}
		for (ProductInCart product : cart.bucketList){
			if(product.orginalProduct.getCategory() == AvailableProductsCategory.POSTER){
				posterList.add(product);
			}
		}
		Collections.sort(posterList, new ProductComparator());
		
		for (int i = 0; i < (posterList.size() / 3); i++) {
			
			String posterToDiscount = posterList.get(i).orginalProduct.getID();
			for (ProductInCart product : cart.bucketList) {
				if (posterToDiscount.equals(product.orginalProduct.getID())) {
					product.priceAfterDiscount = 0;
				}
			}
		}
	}

	private class ProductComparator implements Comparator<ProductInCart> {

		@Override
		public int compare(ProductInCart arg0, ProductInCart arg1) {
			int price = arg0.orginalProduct.getPrice() - arg1.orginalProduct.getPrice();
			if (price == 0) {
				return arg0.compareTo(arg1);
			}
			return price;
		}
	}

	public void discounterCart() {
		cart.cartTotal = 0;
		for (ProductInCart product : cart.bucketList) {
			double productPrice = (double) product.priceAfterDiscount;
			cart.cartTotal = cart.cartTotal + productPrice;
		}
		double tempDiscount = (double)(cart.cartTotal / 100);
		double discountPercent = (double)Math.floor(tempDiscount) / 20;
		if (discountPercent > 0.5) {
			discountPercent = 0.5;
		}
		cart.cartTotalAfterDiscount = (double)cart.cartTotal - (cart.cartTotal*discountPercent);

	}

	// public void discounterGame(){
	// Map<Products.AvailableProducers, List<ProductInCart>> gameMap = new
	// HashMap<>();
	// for (ProductInCart product : cart.bucketList) {
	// if (product.orginalProduct.getCategory() ==
	// AvailableProductsCategory.GAME){
	// Game parsedToGame = (Game) product.orginalProduct;
	// product.priceAfterDiscount = parsedToGame.getPrice();
	// gameMap.put(parsedToGame.getProducer(),
	// gameMap.getOrDefault(parsedToGame.getProducer(), new
	// ArrayList<ProductInCart>());
	// }
	// }
	// }
}
