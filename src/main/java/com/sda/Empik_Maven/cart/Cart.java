package com.sda.Empik_Maven.cart;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.sda.Empik_Maven.products.AvailableProductsCategory;
import com.sda.Empik_Maven.products.Product;
import com.sda.Empik_Maven.shop.Shop;
import com.sda.Empik_Maven.users.Customer;


public class Cart {

	public List<ProductInCart> bucketList;
	public DiscountCalculator discounter = new DiscountCalculator(this);
	public double cartTotal = 0;
	public double cartTotalAfterDiscount;

	public Cart(Customer customer) {
		bucketList = new LinkedList<>();
	}

	public void addToCart(String productID) {
		String orginalID = productID;
		String[] parsed = productID.split("-");
		int categoryID = Integer.parseInt(parsed[0]);
		List<Product> categoryList = null;

		for (Map.Entry<AvailableProductsCategory, LinkedList<Product>> entry : Shop.getInstance().shopProductRange
				.entrySet()) {
			if (entry.getKey().getID() == categoryID) {
				categoryList = entry.getValue();
			}
		}
		if (categoryList == null) {
			System.err.println("No such product in shop product range");
			return;
		}
		for (Product product : categoryList) {
			if (product.getID().equals(orginalID)) {
				bucketList.add(new ProductInCart(product));
				this.ultimateDiscounter(product);
				discounter.discounterCart();
				;
				System.out.println("Product was added to Your cart");
			}
		}
	}

	public void displayCart() {
		System.out.println("Content of Your cart:");
		System.out.println(String.format("%-5s | %-35s | %10s | %15s |", "ID", "Product name", "Price", "Discount price"));
		Iterator it = bucketList.iterator();
		while (it.hasNext()) {
			System.out.println(it.next());
		}
		System.out.println("Your total including all discounts is: $ " + cartTotalAfterDiscount);
	}

	public void ultimateDiscounter(Product product) {
		AvailableProductsCategory category = product.getCategory();
		switch (category) {
		case CD :
			discounter.discounterCDandDVD(product);
			break;
		case DVD:
			discounter.discounterCDandDVD(product);
			break;
		case BOOK:
			discounter.discounterBook(product);
			break;
		case MUG:
			discounter.discounterMug(product);
			break;
		case POSTER:
			discounter.discounterPoster(product);
			break;
		}
	}

}
