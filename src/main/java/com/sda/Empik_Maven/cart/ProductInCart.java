package com.sda.Empik_Maven.cart;

import com.sda.Empik_Maven.products.Product;

public class ProductInCart {
	
	Product orginalProduct;
	double priceAfterDiscount;
	
	public ProductInCart(Product product) {
		orginalProduct = product;
		priceAfterDiscount = orginalProduct.getPrice();
	}
	
	@Override
	public String toString(){
		return String.format("%s | $ %13.2f |",orginalProduct.toString(), priceAfterDiscount);
	}

	public int compareTo(ProductInCart arg1) {
		int compareName = this.orginalProduct.getName().compareTo(arg1.orginalProduct.getName());
		
		if(compareName == 0){
			compareName = 1;
		}
		
		return compareName;
	}
	
	

}
