package com.sda.Empik_Maven.shop;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

import com.sda.Empik_Maven.products.AvailableProductsCategory;
import com.sda.Empik_Maven.products.Product;
import com.sda.Empik_Maven.users.Admin;
import com.sda.Empik_Maven.users.Customer;



public class Shop {
	
	private static Shop instance;
	public Map<AvailableProductsCategory, LinkedList<Product>> shopProductRange;	
	public Map<Integer, Customer> Customers = new HashMap<>();
	
	public static Shop getInstance(){
		if(instance == null){
			instance = new Shop();
		}
		return instance;
	}
	
	public Shop(){
		shopProductRange = new TreeMap<>();
	}
	
	public void addNewProductToShopProductRange(Product givenProduct, Admin adminUser){
		if(adminUser == null){
			return;
		}
		AvailableProductsCategory productCategory = givenProduct.getCategory();
		if(!shopProductRange.containsKey(productCategory)){
			shopProductRange.put(productCategory, new LinkedList<Product>());
		}
		shopProductRange.get(productCategory).add(givenProduct);
	}
}
